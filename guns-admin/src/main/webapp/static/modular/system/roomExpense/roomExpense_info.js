/**
 * 初始化详情对话框
 */
var RoomExpenseInfoDlg = {
    roomExpenseInfoData : {}
};

/**
 * 清除数据
 */
RoomExpenseInfoDlg.clearData = function() {
    this.roomExpenseInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomExpenseInfoDlg.set = function(key, val) {
    this.roomExpenseInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomExpenseInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RoomExpenseInfoDlg.close = function() {
    parent.layer.close(window.parent.RoomExpense.layerIndex);
}

/**
 * 收集数据
 */
RoomExpenseInfoDlg.collectData = function() {
    this
    .set('unit')
    .set('price')
    .set('name')
    .set('valuationtype')
    .set('price')
    .set('unit');
}

/**
 * 提交添加
 */
RoomExpenseInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/roomExpense/add", function(data){
        Feng.success("添加成功!");
        window.parent.RoomExpense.table.refresh();
        RoomExpenseInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roomExpenseInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RoomExpenseInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/roomExpense/update", function(data){
        Feng.success("修改成功!");
        window.parent.RoomExpense.table.refresh();
        RoomExpenseInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roomExpenseInfoData);
    ajax.start();
}

$(function() {

});
