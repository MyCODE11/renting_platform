/**
 * 管理初始化
 */
var Room = {
    id: "RoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Room.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '房间对象', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '房间名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '房间编码', field: 'buildCode', visible: true, align: 'center', valign: 'middle'},
        {title: '面积', field: 'roomArea', visible: true, align: 'center', valign: 'middle'},
        {title: '月租金', field: 'monthRent', visible: true, align: 'center', valign: 'middle'},
        {title: '付款方式', field: 'valuationType', visible: true, align: 'center', valign: 'middle'},
        {title: '到期时间', field: 'endDate', visible: true, align: 'center', valign: 'middle'},
        {title: '押金', field: 'pledgeCash', visible: true, align: 'center', valign: 'middle'},
        /* {title: '房间图片', field: 'roomImage', visible: true, align: 'center', valign: 'middle'},*/
        {title: '费用清单', field: 'expenseStr', visible: true, align: 'center', valign: 'middle'},
        {title: '房屋状态', field: 'is_rent', visible: true, align: 'center', valign: 'middle'},
        /* {title: '经纪人', field: 'broker', visible: true, align: 'center', valign: 'middle'},*/
    ];
};

var isrent;

function selectOnchange(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    isrent = value;
    // console.log(value)
    Room.search()
}


/**
 * 检查是否选中
 */
Room.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Room.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Room.openAddRoom = function () {
    var index = layer.open({
        type: 2,
        title: '添加房间',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/room/room_add'
    });
    this.layerIndex = index;
};

// 修改

Room.openChangeRoom = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '修改房间',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/room/room_edit/' + this.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开查看详情
 */
Room.openRoomDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/room/room_edit/' + Room.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Room.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/room/delete", function (data) {
            Feng.success("删除成功!");
            Room.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("roomId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
Room.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['isRent'] = isrent;
    Room.table.refresh({query: queryData});
};

// 清单费用
Room.openAddRoomExpense = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/roomExpense/roomExpense_add'
    });
    this.layerIndex = index;
};

$(".bs-checkbox ").each(function () {
    $(this).find("input[type=checkbox]:checked");
})
$(function () {
    var typeList = {
        "5": "押一付一",
        "6": "押一付二",
        "7": "押一付三",
        "8": "押一付四",
        "9": "押一付五",
        "10": "押一付六",
        "11": "押一付七",
        "12": "押一付八",
        "13": "押一付九",
        "14": "押一付十",
        "15": "押一付十一",
        "16": "押一付十二",
    };
    var defaultColunms = Room.initColumn();
    var table = new BSTable(Room.id, "/room/selectRoomForManage", defaultColunms);

    // table.setExpandColumn(2);
    // table.setExpandAll(true);
    table.setPaginationType("client");
    Room.table = table.init();


    $("#isRent").find("option").each(function () {
        // console.log($(this).text())
        if ($(this).text() == "未出租") {
            $(this).attr("value", "0");
        } else if ($(this).text() == "已出租") {
            $(this).attr("value", "1");
        } else {
            $(this).attr("value", "");
        }
    })

});
