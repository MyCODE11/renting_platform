var RoomInfoDlg = {
    roomInfoData: {}
};

/**
 * 清除数据
 */
RoomInfoDlg.clearData = function () {
    this.roomInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomInfoDlg.set = function (key, val) {
    this.roomInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RoomInfoDlg.close = function () {
    parent.layer.close(window.parent.Room.layerIndex);
}

/**
 * 收集数据
 */
RoomInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('buildCode')
        .set('roomNumber')
        .set('roomArea')
        .set('sortNumber')
        .set('floor')
        .set('useType')
        .set('roomType')
        .set('decorationType')
        .set('rentType')
        .set('monthRent')
        .set('pledgeCash')
        .set('roomImage')
        .set('expenseStr')
        .set('is_rent')
        .set('broker')

}

function collectExpense() {
    //收集费用信息
    //返回

}

/**
 * 提交修改
 */
RoomInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();
    var date;
    date = $("#date").val().split("-").join("/");
    this.roomInfoData['startDate'] = date;
    //数据初始化，数据未进行修改将默认数据存储传输给后台
        $("#abc").find('tr').each(function () {
            var text1 = $(this).find("td").eq(1).text();
            var text2 = $(this).find("td").eq(2).text();
            var text3 = $(this).find("td").eq(3).text();
            var text4 = $(this).find("td").eq(4).text();
            var indexs = $(this).index();
            console.log(text1);
            var Id = $("#id").val();
            var obj = new expenseList(indexs, Id, text1, text2, text3, text4);
            expens.push(obj);
        })
    console.log(expens)
    //费用清单数据去重
    for (var i = 0; i < expens.length; i++) {
        for (var j = i + 1; j < expens.length;) {
            if (expens[i].index == expens[j].index) {//通过id属性进行匹配；
                expens.splice(j, 1);//去除重复的对象；
            } else {
                j++;
            }
        }
    }
    this.roomInfoData["expens"] = listToJson(expens);
    //资产进行判断，未修改将初始数据传输给后台
    var propertyList = new Array();
    $("#propertyTable").find('table').find('#propertyBox').find("tr").each(function () {
        var propertyId = $(this).attr("id");
        var propertyName = $(this).find('td').eq(1).text();
        var propertyNum = $(this).find('td').eq(2).text();
        propertyList.push(new propertyObj(propertyId, propertyName, propertyNum)) ;
    })
    this.roomInfoData["propertyList"] = listToJson(propertyList);
    this.roomInfoData["isRent"] = isRent;
    if(name == ""){
        $("#name").focus();
        Feng.error("房间名称不能为空")
        return;
    }else if(buildCode == ""){
        $("#buildCode").focus();
        Feng.error("房间编码不能为空")
        return;
    }else if(propertyList.length == 0){
        Feng.error("房间资产不能为空")
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/room/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Room.table.refresh();
        RoomInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roomInfoData);
    ajax.start();
}

//转换json数据
function listToJson(list) {
    var listJson = JSON.stringify(list);
    return listJson;
}

//费用清单 封装一个构造函数

function expenseList(index, roomId, name, price, unit, valuationType) {
    this.index = index;
    this.roomId = roomId;
    this.name = name;
    this.price = price;
    this.unit = unit;
    this.valuationType = valuationType;
}

var expens = new Array();


//点击修改清单时创建对象，将数据插入到对象，对象插入数组

$("#editExpense").click(function () {
    editClick()
});

//房间资产构造函数
function propertyObj(id, name, number) {
    this.id = id;
    this.name = name;
    this.number = number;
}

//获取名字属性
//获取费用单价
//获取费用计量单位
var editName = "";
var editValuation_type = "";
var editUnit = "";
var isRent = $("#toState").val();

//修改页面增加费用初始化
var addName = "";
var addUnit = "";
var addValuation = "";

//获取费用名称下拉框值
function selectOnchange1(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    editName = value;
    if(editName == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#valuationType").empty();
        $("#valuationType").append(list);

        $("#addValuationType").empty();
        $("#addValuationType").append(list);
    }else{
        var elseList = "<option value='1'>按房间一次性收费</option>" +
            "<option value='2'>按房间每月收费</option>" +
            "<option value='3'>按房间每日收费</option>" +
            "<option value='4'>抄表各房间独立结算</option>" +
            "<option value='17'>其他类型</option>";
        $("#valuationType").empty();
        $("#valuationType").append(elseList);

        $("#addValuationType").empty();
        $("#addValuationType").append(elseList);
    }

    var oDiv = document.getElementById("application");
    if (value == "维修费") {
        oDiv.style.display = "block";
    } else {
        oDiv.style.display = "none";
    }
    console.log(editName)
    addName = value;
    console.log(addName)
}

//触发事件判断
function triggerEvent() {
    if(addName == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#valuationType").empty();
        $("#valuationType").append(list);
    }else{
        var elseList = "<option value='1'>按房间一次性收费</option>" +
            "<option value='2'>按房间每月收费</option>" +
            "<option value='3'>按房间每日收费</option>" +
            "<option value='4'>抄表各房间独立结算</option>" +
            "<option value='17'>其他类型</option>";
        $("#valuationType").empty();
        $("#valuationType").append(elseList);
    }
}

//获取计价方式下拉框
function selectOnchange2(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    editUnit = value;
    console.log(editUnit)
    addUnit = value;

}

//获取计量单位下拉框
function selectOnchange3(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    editValuation_type = value;
    console.log(editValuation_type)
    addValuation = value;
}

//获得房屋状态下拉框
function selectOnchange(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    console.log(value)
    isRent = value;
}
var editPropertyName = '';
var editPropertyId = '';
//添加财产名称下拉框选中数据
function selectOnchanges(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    var id = obj.options[obj.selectedIndex].id;
    editPropertyName = value;
    editPropertyId = id;
    console.log(editPropertyName)
}
//点击表格中的数据修改资产的数据
var propertyAgainName = '';
var propertyAgainId = '';
function editSelectOnchange(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    var id = obj.options[obj.selectedIndex].id;
    propertyAgainName = value;
    propertyAgainId = id;
    console.log(propertyAgainName);
}


//点击修改按钮变更表格中的内容，表格中内容与下拉框选中内容相同，在点击中调用
function editClick() {
    var html = $("#abc").find("tr").eq(index);
    $(html).find("td").eq(1).text(editName);
    $(html).find("td").eq(3).text(editUnit);
    $(html).find("td").eq(2).text($("#price").val());
    $(html).find("td").eq(4).text(editValuation_type);
    $("#money").css("display", "none");
}

$("#addExpense").click(function () {
    $("#addMoney").css("display", "block");
    $("#money").css("display", "none");
    $("#abc").find('tr').each(function () {
        var addtrName = $(this).find('td').eq(1).text();
        $('#addName').find('option').each(function () {
            if($(this).val() == addtrName){
                $(this).css('display', 'none');
                $(this).css('selected',  false);
                $('#addName').val($(this).next().val());

            }
            addName = $('#addName').val();
            triggerEvent();
        })
        $('#addPrice').val('');

    })

})

$("#off").click(function(){
    $("#money").css("display", "none")
})
$("#addOff").click(function(){
    $("#addMoney").css("display", "none")
})

$("#add").click(function () {
    var Name = $("#addName").val();
    var Valuation = $("#addValuationType").val();
    var Unit = $("#addUnit").val();
    var addPrice = $("#addPrice").val();
    if(addPrice == ''){
        alert("请输入单价！")
    }else{
        var len=$("#abc tr").length;
        $("#table").find('table').append("<tr id="+len+">"+"<td>"+"<input id='radio' type='radio' name='optionsRadiosinline'>"+"</td>"+"<td>"+Name+"</td>"+"<td>"+addPrice+"</td>"+"<td>"+Unit+"</td>"+"<td>"+Valuation+"</td>"+"<td>"+"<a class='delete' href='javascript:;'>"+"删除"+"</a>"+"</td>"+"</tr>");
        $("#addMoney").css("display", "none")
        console.log($("#abc tr").length)
    }
    $("#table").find('table').find(".delete").each(function () {
        $(this).click(function(){
            $(this).parent().parent().remove();
        })
    })

})
//点击添加资产按钮时让资产框显示
$("#addpropertyBtn").click(function () {
    $("#propertyList").css("display", "block");
    $("#propertyListedit").css("display", "none");
    $("#propertyTable").find("#propertyBox").find("tr").each(function(){
        var trname = $(this).find('td').eq(1).text();
        console.log(trname);
        $("#property").find('option').each(function () {
            //点击增加资产时，若下拉框存在与表格中数据相同的则让其隐藏
            if($(this).val() == trname){
                $(this).css("display", "none");
                $("#property").val($(this).next().val());
            }
            editPropertyName = $("#property").val();
            if($(this).val() == editPropertyName){
                editPropertyId = $(this).attr('id');
            }
        })
    })
})
//添加房间资产，若数量为空做出提示
$("#newAddProperty").click(function () {
    var num = $("#propertyNum").val();
    var timer;
    if(num == ''){
        //提示框
        $(".popup").show();
        timer = setInterval(function () {
            $(".popup").hide();
        }, 1000)
    }else{
        $("#propertyList").css("display", "none");
        clearInterval(timer);
        var number = $("#propertyNum").val();
        //不为空添加一行
        $("#propertyTable").find('table').append("<tr id="+editPropertyId+">"+"<td>"+"<input id='editRadio' type='radio' name='optionsRadiosinline'>"+"</td>"+"<td>"+editPropertyName+"</td>"+"<td>"+number+"</td>"+"<td>"+"<a class='delete' href='javascript:;'>"+"删除"+"</a>"+"</td>"+"</tr>");
    }
    $("#propertyTable").find('table').find(".delete").each(function () {
        $(this).click(function(){
            $(this).parent().parent().remove();
        })
    })

})
//点击表格中删除按钮删除此行
$("#propertyBox").find("tr").find(".delBtn").each(function () {
    $(this).click(function(){
        $(this).parent().parent().remove();
    })
})
//获取表格每一行的下标
var trId = "";
//遍历一下房间资产表格的每一行
$("#propertyTable").find("#propertyBox").find("tr").each(function(){
    $(this).find("#pBtn").click(function () {
        //点击修改让修改资产框显示
        $("#propertyListedit").css("display", "block");
        //让增加资产框隐藏
        $("#propertyList").css("display", "none");
        //获取每一行的房间资产与资产数量
        var trName = $(this).parent().parent().find('td').eq(1).text();
        var trNum = $(this).parent().parent().find('td').eq(2).text();
        //遍历下拉框中的数据
        $("#propertyEdit").find("option").each(function () {
            //若下拉框中的数据等于表格中的数据让他显示到下拉框中
            if($(this).val() == trName){
                $(this).attr("selected", true);
                $(this).siblings().attr("selected", false);

            }
            //让下拉框触发就得数据等于下拉框数据
            propertyAgainName = $("#propertyEdit").val();

        })

        $("#editpropertyNum").val(trNum);
        //触发后数据与表格内容相同，而触发后的数据id也与表格内容的id相同
        if(trName == propertyAgainName){
            propertyAgainId = $(this).parent().parent().attr("id");
        }
    })
})

//点击修改之后将修改框中的下拉框数据加载到表格中
$('#newEditProperty').click(function () {
    $("#propertyTable").find("#propertyBox").find("tr").eq(trId).attr("id", propertyAgainId);
    $("#propertyTable").find("#propertyBox").find("tr").eq(trId).find('td').eq(1).text(propertyAgainName);
    $("#propertyTable").find("#propertyBox").find("tr").eq(trId).find('td').eq(2).text($("#editpropertyNum").val());
    $("#propertyListedit").css("display", "none");

})
//点击右上角x让div隐藏
$("#editOff").click(function () {
    $("#propertyListedit").css("display", "none");
})
$("#addPoff").click(function () {
    $("#propertyList").css("display", "none");
})
//修改页面中获取下拉框的值
//设置全局变量费用名称、单价、计量单位、计价方式
var N;
var P;
var U;
var V;
var index = 0;
var Id;
var extent;

//获取时间
$(function () {

    Id = $("#id").val();
    // console.log(Id)

    //修改页面的费用清单获取radio单选框,点击添加选中属性
    $("#abc").find("tr").find("#radio").each(function () {
        $(this).click(function () {
            $(this).attr("checked", true);
        })
    })


    //获取页面中tr个数
    $("#abc").find('tr').each(function () {
        extent = $("#abc").find('tr').length;
        console.log(extent)

    })


    //修改页面中修改费用部分
    //修改页面中费用清单表格，点击显示
    $("#abc").find("tr").find("#btn").each(function () {
        $(this).click(function () {
            $("#money").css("display", "block");
            $("#addMoney").css("display", "none");
            N = $(this).parent().parent().find(".editName").text();
            P = $(this).parent().parent().find(".editPrice").text();
            U = $(this).parent().parent().find(".editUnit").text();
            V = $(this).parent().parent().find(".editValuation").text();
            $("#price").val(P);
            //初始化值，下拉框的值默认为表格中的数据
            // console.log(N)
            // editName = N;
            if(N == "月租"){
                var list = "<option value='5'>押一付一</option>"+
                    "<option value='6'>押一付二</option>"+
                    "<option value='7'>押一付三</option>"+
                    "<option value='8'>押一付四</option>"+
                    "<option value='9'>押一付五</option>"+
                    "<option value='10'>押一付六</option>"+
                    "<option value='11'>押一付七</option>"+
                    "<option value='12'>押一付八</option>"+
                    "<option value='13'>押一付九</option>"+
                    "<option value='14'>押一付十</option>"+
                    "<option value='15'>押一付十一</option>"+
                    "<option value='16'>押一付十二</option>";
                $("#valuationType").empty();
                $("#valuationType").append(list);
            }else{
                var elseList = "<option value='1'>按房间一次性收费</option>" +
                    "<option value='2'>按房间每月收费</option>" +
                    "<option value='3'>按房间每日收费</option>" +
                    "<option value='4'>抄表各房间独立结算</option>" +
                    "<option value='17'>其他类型</option>";
                $("#valuationType").empty();
                $("#valuationType").append(elseList);
            }
            editName  = N;
            editValuation_type = V;
            editUnit = U;
            $("#expenses").find("option").each(function () {
                if ($(this).val() == N) {
                    $(this).attr("selected", true);
                } else {
                    $(this).attr("selected", false);
                }
                ;
            })
            $("#money").find("select").each(function () {
                $(this).find("option").each(function () {
                    if ($(this).val() == N) {
                        $(this).attr("selected", true);
                    } else if ($(this).val() == U) {
                        $(this).attr("selected", true);
                    } else if ($(this).val() == V) {
                        $(this).attr("selected", true);
                    } else {
                        $(this).attr("selected", false);
                    }
                    ;
                })
            })

            //获取当前点击按钮所在tr的下标
            index = $(this).parent().parent().index();
        })

    })
    $("#abc").find("tr").find(".delBtn").each(function () {
        $(this).click(function(){
            $(this).parent().parent().remove();
        })
    })

    //修改页面，资产根据状态码status判断是否有此资产，有选中多选框，status=>1 选中 status=>不选中
    $.ajax({
        method: "get",
        url: Feng.ctxPath + "/roomProperty/selectAllProperty",
        cache: false,
        success: function (data) {
            console.log(data)
            var html = "";
            for (var i = 0; i < data.length; i++) {
                // html += `<label id = "${data[i].id}" class="checkbox-inline">
                //             <input type="checkbox" id="inlineCheckbox1" value="option1"> ${data[i].name}
                //         </label>`
                html += `<option id = "${data[i].id}">${data[i].name}</option>`
            }
            $('#property').append(html);
            $("#propertyEdit").append(html);
            editPropertyName = $('#property').find("option").val();
            editPropertyId = $('#property').find("option").attr("id");

            propertyAgainName = $('#propertyEdit').find("option").val();
            propertyAgainId = $('#propertyEdit').find("option").attr("id");
        },
        error: function () {
            console.log("失败");
        }
    })

    $("#isRent").find("option").each(function () {
        if ($(this).val() == $("#toState").val()) {
            $(this).attr("selected", true)
        }
    })


    //在页面上添加上从后台获取的时间
    var date = $("#date").attr("name");
    var now = new Date(date);
    //格式化日，如果小于9，前面补0
    var day = ("0" + now.getDate()).slice(-2);
    //格式化月，如果小于9，前面补0
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    //拼装完整日期格式
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    // 完成赋值
    $('#date').val(today);

    if($("#addMoney").find("#addName").val() == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#addValuationType").empty();
        $("#addValuationType").append(list);
    }else{
        var elseList = "<option value='1'>按房间一次性收费</option>" +
            "<option value='2'>按房间每月收费</option>" +
            "<option value='3'>按房间每日收费</option>" +
            "<option value='4'>抄表各房间独立结算</option>" +
            "<option value='17'>其他类型</option>";
        $("#addValuationType").empty();
        $("#addValuationType").append(elseList);
    }
    //获取房间资产表格每一行的下标
    $("#propertyTable").find("#propertyBox").find("tr").each(function() {
        $(this).find("#pBtn").click(function () {
            trId = $(this).parent().parent().index();
        })
    })

})


