/**
 * 管理初始化
 */
var RawRoom = {
    id: "RawRoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
RawRoom.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '主键', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '房间名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '房间面积', field: 'roomArea', visible: true, align: 'center', valign: 'middle'},
        // {title: '房间图片', field: 'roomImage', visible: true, align: 'center', valign: 'middle'},
        {title: '房间户型', field: 'roomType', visible: true, align: 'center', valign: 'middle'},
        {title: '月租金', field: 'monthRent', visible: true, align: 'center', valign: 'middle'},
        {title: '付款方式', field: 'valuationType', visible: true, align: 'center', valign: 'middle'},
        {title: '押金', field: 'pledgeCash', visible: true, align: 'center', valign: 'middle'},
        /*{title: '房屋种类', field: 'room_kind', visible: true, align: 'center', valign: 'middle'},*/
       /* {title: '房屋状态', field: 'is_rent', visible: true, align: 'center', valign: 'middle'},*/
        {title: '起租日期', field: 'startDate', visible: true, align: 'center', valign: 'middle'},
        {title: '租赁结束日期', field: 'endDate', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
RawRoom.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        RawRoom.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
RawRoom.openAddRawRoom = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/rawRoom/rawRoom_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
RawRoom.openRawRoomDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/rawRoom/room_edit/' + RawRoom.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
RawRoom.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/rawRoom/delete", function (data) {
            Feng.success("删除成功!");
            RawRoom.table.refresh();
        }, function (data) {
            Feng.error("删除失败!");
        });
        ajax.set("roomId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
RawRoom.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    RawRoom.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = RawRoom.initColumn();
    var table = new BSTable(RawRoom.id, "/rawRoom/selectRoomForManage", defaultColunms);
    table.setPaginationType("client");
    RawRoom.table = table.init();
});
