/**
 * 初始化详情对话框
 */
var RawRoomInfoDlg = {
    rawRoomInfoData : {}
};

/**
 * 清除数据
 */
RawRoomInfoDlg.clearData = function() {
    this.rawRoomInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RawRoomInfoDlg.set = function(key, val) {
    this.rawRoomInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RawRoomInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RawRoomInfoDlg.close = function() {
    parent.layer.close(window.parent.RawRoom.layerIndex);
}

/**
 * 收集数据
 */
RawRoomInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('roomArea')
    .set('roomType')
    .set('monthRent')
    .set('pledgeCash')
    .set('room_kind')
    .set('is_rent')
    .set('startDate')
    .set('endDate');
}

/**
 * 提交添加
 */
RawRoomInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    var date;
    if ($("#date").val() == '') {
        // 获取当前日期
        date = (new Date()).toLocaleDateString();
    } else {
        date = $("#date").val().split("-").join("/");
    };
    var name = $("#name").val();
    var buildCode = $("#buildCode").val();
    console.log(date)
    this.rawRoomInfoData["name"] = name;
    // this.rawRoomInfoData["buildCode"] = buildCode;
    this.rawRoomInfoData["expens"] = listToJson(expenseList);
    this.rawRoomInfoData["propertyList"] = listToJson(propertyList);
    this.rawRoomInfoData["isRent"] = isRent;
    this.rawRoomInfoData['startDate'] = date;
    this.rawRoomInfoData['roomKind'] = 1;

    if(name == ""){
        $("#name").focus();
        Feng.error("房间名称不能为空" + "!");
        return;
    }else if(propertyList.length == 0){
        Feng.error("房间资产不能为空" + "!");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/rawRoom/add", function(data){
        Feng.success("添加成功!");
        window.parent.RawRoom.table.refresh();
        RawRoomInfoDlg.close();
    },function(data){
        Feng.error("添加失败");
    });
    ajax.set(this.rawRoomInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RawRoomInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/rawRoom/update", function(data){
        Feng.success("修改成功!");
        window.parent.RawRoom.table.refresh();
        RawRoomInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.rawRoomInfoData);
    ajax.start();
}
//获取名字属性
//获取费用单价
//获取费用计量单位
var name = "";
var valuation_type = '';
var unit = "";
var isRent = '';
var propertyName = '';
var propertyId = '';

//房屋状态
// var use = "";
//获取费用名称下拉框值
function selectOnchang1(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;

    if(value == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#valuationType").empty();
        $("#valuationType").append(list);
    }else{
        var elseList = "<option value='1'>按房间一次性收费</option>" +
            "<option value='2'>按房间每月收费</option>" +
            "<option value='3'>按房间每日收费</option>" +
            "<option value='4'>抄表各房间独立结算</option>" +
            "<option value='17'>其他类型</option>";
        $("#valuationType").empty();
        $("#valuationType").append(elseList);
    }

    name = value;
    var oDiv = document.getElementById("application");
    if (value == "维修费") {
        oDiv.style.display = "block";
    } else {
        oDiv.style.display = "none";
    }
    console.log(name);
}


//获取计价方式下拉框
function selectOnchange(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    isRent = value;
    console.log(isRent)
}
function selectOnchang2(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    unit = value;
}
//获取计量方式下拉框
function selectOnchang3(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    valuation_type = value;
}

var i = 0;
//添加页面 费用相关
var expenseList = new Array();
var addPrice;
$("#addExpense").click(function () {
    //创建费用对象
    if ($("#price").val() == '') {
        alert('单价不能为空')
    } else {
        var expense = new Object();
        expense.roomId = i++;
        expense.name = name;
        expense.valuation_type = valuation_type;
        expense.price = $("#price").val();
        expense.unit = unit;
        expense.remark = $("#use").val();
        //添加到费用集合
        expenseList.push(expense);
        console.log(expenseList);
    }

    showExpense();
});

//获取资产下拉框值
function selectOnchanges(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    var id = obj.options[obj.selectedIndex].id;
    propertyName = value;
    propertyId = id;
    console.log(propertyId)
}

//转换集合为json格式
function listToJson(list) {
    var listJson = JSON.stringify(list);
    return listJson;
}


//确定添加费用对象按钮的点击事件，将创建费用对象并添加到集合中
//控制显示费用清单div的方法
function showExpense() {
    var div = document.getElementById('expenseDiv');
    var oDiv = document.getElementById("application");
    var expen = document.getElementById("roomExpenseList");
    if (expenseList.length != 0) {
        var text = "";
        for (item in expenseList) {
            var names = expenseList[item].name;
            var units = expenseList[item].unit;
            var valuation_types = expenseList[item].valuation_type;
            var prices = expenseList[item].price;
            if (oDiv.style.display == "block") {
                var use = expenseList[item].remark;
                if (use) {
                    text += names + "(" + use + ")" + "  " + "￥" + prices + " " + units + " --- " + valuation_types + "  ";
                }
            } else {
                text += names + "  " + "￥" + prices + " " + units + " --- " + valuation_types + "  ";
            }
            $("#expenses").find("option").each(function () {
                if($(this).val() == names){
                    $(this).css("display", "none");
                    $(this).css('selected',  false);
                    $("#expenses").val($(this).next().val());
                }
                name = $("#expenses").val();
                triggerEvent();
            })
            valuation_type = $("#valuationType").val();
            unit = $("#unit").val();
            $("#price").val("");
        }
        expen.value = text;
    } else {
        //隐藏
        div.style.display = 'none';
    }
}


//1.创建构造函数
function nameObject(Id, name, num) {
    this.id = Id;
    this.name = name;
    this.number = num;
}
//触发事件判断
function triggerEvent() {
    if(name == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#valuationType").empty();
        $("#valuationType").append(list);
    }else{
        var elseList = "<option value='1'>按房间一次性收费</option>" +
            "<option value='2'>按房间每月收费</option>" +
            "<option value='3'>按房间每日收费</option>" +
            "<option value='4'>抄表各房间独立结算</option>" +
            "<option value='17'>其他类型</option>";
        $("#valuationType").empty();
        $("#valuationType").append(elseList);
    }
}

//点击添加资产按钮
var propertyList = new Array();
$("#addProperty").click(function () {
    var num = $("#propertyPrice").val();
    if(num == ''){
        $(".popup").show();
        timer = setInterval(function () {
            $(".popup").hide();
        }, 1000)
    }else{
        propertyList.push(new nameObject(propertyId, propertyName , num));
        $(".popup").show();
        timer = setInterval(function () {
            $(".popup").hide();
        }, 1000)
        $(".popup").find("p").find("span").eq(1).text("添加成功！")
        $(".popup").find("p").find("span").eq(0).attr("class", "iconfont icon-chenggong");
        $(".popup").find("p").find(".icon-chenggong").css("font-size", "20px");
        $(".popup").find("p").find(".icon-chenggong").css("color", "red");
        console.log(propertyList)
    }
    showProperty();
})
//文本框中添加资产
function showProperty(){
    if(propertyList.length != 0){
        var text = '';
        for(var num in propertyList){
            var id = propertyList[num].id;
            var name = propertyList[num].name;
            var number = propertyList[num].number;
            text += name + "(" + number + ")" + "  ";
            $("#property").find('option').each(function () {
                if($(this).val() == name){
                    $(this).css("display", "none");
                    $(this).css('selected',  false);
                    $("#property").val($(this).next().val());

                }

                propertyName = $("#property").val();
                if($(this).val() == propertyName){
                    propertyId = $(this).attr('id');
                }
            })
            console.log(id)
            $("#propertyPrice").val('');
        }
        $("#roomPropertyList").val(text);

    }

}



$(function () {

    //添加页面初始化费用的属性值
    name = $("#expenses").val();
    valuation_type = $("#valuationType").val();
    unit = $("#unit").val();


    //添加页面中获取房间资产数据
    $.ajax({
        method: "get",
        url: Feng.ctxPath + "/roomProperty/selectAllProperty",
        cache: false,
        success: function (data) {
            console.log(data)
            var html = "";
            for (var i = 0; i < data.length; i++) {
                html += `<option id = "${data[i].id}">${data[i].name}</option>`
            }
            $("#propertyBox").find('#property').append(html);
            propertyName = $("#propertyBox").find('#property').find("option").val();
            propertyId = $("#propertyBox").find('#property').find("option").attr("id");
            console.log(propertyId);
        },
        error: function () {
            console.log("失败");
        }
    })

    $("#rentType").find("option").each(function () {
        if ($(this).attr("selected")) {
            var isState = $(this).val()
        };
        isRent = isState;
    })

    if($("#money").find("#expenses").val() == "月租"){
        var list = "<option value='5'>押一付一</option>"+
            "<option value='6'>押一付二</option>"+
            "<option value='7'>押一付三</option>"+
            "<option value='8'>押一付四</option>"+
            "<option value='9'>押一付五</option>"+
            "<option value='10'>押一付六</option>"+
            "<option value='11'>押一付七</option>"+
            "<option value='12'>押一付八</option>"+
            "<option value='13'>押一付九</option>"+
            "<option value='14'>押一付十</option>"+
            "<option value='15'>押一付十一</option>"+
            "<option value='16'>押一付十二</option>";
        $("#valuationType").empty();
        $("#valuationType").append(list);
    }
    valuation_type = $("#valuationType").val();
});

