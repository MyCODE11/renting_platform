/**
 * 管理初始化
 */
var RawRoomExpense = {
    id: "RawRoomExpenseTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
RawRoomExpense.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '对应的房间对象的ID', field: 'roomId', visible: true, align: 'center', valign: 'middle'},
            {title: '费用名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '费用计价方式：1-按房间一次性收费；2-按房间每月收费；3-按房间每日收费；4-抄表各房间独立结算；5-其他类型；', field: 'valuationType', visible: true, align: 'center', valign: 'middle'},
            {title: '费用单价', field: 'price', visible: true, align: 'center', valign: 'middle'},
            {title: '费用计量单位', field: 'unit', visible: true, align: 'center', valign: 'middle'},
            {title: '费用备注', field: 'remark', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
RawRoomExpense.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        RawRoomExpense.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
RawRoomExpense.openAddRawRoomExpense = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/rawRoomExpense/rawRoomExpense_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
RawRoomExpense.openRawRoomExpenseDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/rawRoomExpense/rawRoomExpense_update/' + RawRoomExpense.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
RawRoomExpense.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/rawRoomExpense/delete", function (data) {
            Feng.success("删除成功!");
            RawRoomExpense.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("rawRoomExpenseId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
RawRoomExpense.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    RawRoomExpense.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = RawRoomExpense.initColumn();
    var table = new BSTable(RawRoomExpense.id, "/rawRoomExpense/list", defaultColunms);
    table.setPaginationType("client");
    RawRoomExpense.table = table.init();
});
