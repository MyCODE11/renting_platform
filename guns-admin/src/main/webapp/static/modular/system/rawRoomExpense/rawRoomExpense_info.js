/**
 * 初始化详情对话框
 */
var RawRoomExpenseInfoDlg = {
    rawRoomExpenseInfoData : {}
};

/**
 * 清除数据
 */
RawRoomExpenseInfoDlg.clearData = function() {
    this.rawRoomExpenseInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RawRoomExpenseInfoDlg.set = function(key, val) {
    this.rawRoomExpenseInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RawRoomExpenseInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RawRoomExpenseInfoDlg.close = function() {
    parent.layer.close(window.parent.RawRoomExpense.layerIndex);
}

/**
 * 收集数据
 */
RawRoomExpenseInfoDlg.collectData = function() {
    this
    .set('id')
    .set('roomId')
    .set('name')
    .set('valuationType')
    .set('price')
    .set('unit')
    .set('remark');
}

/**
 * 提交添加
 */
RawRoomExpenseInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/rawRoomExpense/add", function(data){
        Feng.success("添加成功!");
        window.parent.RawRoomExpense.table.refresh();
        RawRoomExpenseInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.rawRoomExpenseInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RawRoomExpenseInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/rawRoomExpense/update", function(data){
        Feng.success("修改成功!");
        window.parent.RawRoomExpense.table.refresh();
        RawRoomExpenseInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.rawRoomExpenseInfoData);
    ajax.start();
}

$(function() {

});
