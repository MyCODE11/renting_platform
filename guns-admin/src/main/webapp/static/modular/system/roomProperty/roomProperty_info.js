/**
 * 初始化详情对话框
 */
var RoomPropertyInfoDlg = {
    roomPropertyInfoData: {}
};

/**
 * 清除数据
 */
RoomPropertyInfoDlg.clearData = function () {
    this.roomPropertyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomPropertyInfoDlg.set = function (key, val) {
    this.roomPropertyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoomPropertyInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RoomPropertyInfoDlg.close = function () {
    parent.layer.close(window.parent.RoomProperty.layerIndex);
}

/**
 * 收集数据
 */
RoomPropertyInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name');
}

//转换集合为json格式
function listToJson(list) {
    var listJson = JSON.stringify(list);
    return listJson;
}

/**
 * 提交添加
 */
RoomPropertyInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();
    var record = addPayTime();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/paymentRecord/add", function (data) {
        console.log(data);
        Feng.success("添加成功!");
        window.parent.RoomProperty.table.refresh();
        RoomPropertyInfoDlg.close();
    }, function (data) {
        this.clearData();
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(record);
    ajax.start();
}
/**
 * 提交修改
 */
RoomPropertyInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/roomProperty/update", function (data) {
        Feng.success("修改成功!");
        window.parent.RoomProperty.table.refresh();
        RoomPropertyInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roomPropertyInfoData);
    ajax.start();
}

var count;

function selectOnchange(obj) {
    //获取被选中的option标签选项
    var value = obj.options[obj.selectedIndex].value;
    console.log(value)
    count = value;
}

//点击一个房间或去哪一个房间的id



var value = $(".payCost").val();
function addPayTime() {

    if (value == '') {
        value = new Date().toLocaleDateString();
    } else {
        value = $(".payCost").val().split("-").join("/")
    }
    var payTimeObj = {};
    var i = 0;
    payTimeObj.id = i++;
    payTimeObj.roomId = payId;
    payTimeObj.payTime = value;
    payTimeObj.count = $(".month").val();
    payTimeObj.payTime = value;
    payTimeObj.expenseName = $(".payText").val();
    payTimeObj.amount = $(".price").val();
    return payTimeObj;

}


// 获取主页信息
var payId;
$(function () {

    $(".addpayTime").each(function(){
        $(this).click(function () {
            payId = $(this).parent().parent().parent().attr("id");
        })
    })

});
