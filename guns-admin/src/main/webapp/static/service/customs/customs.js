/**
 * 日志管理初始化
 */
var customs = {
    id: "customsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
customs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'logname',  align: 'center', valign: 'middle'},
        {title: '公司名称', field: 'logname', align: 'center', valign: 'middle'},
        {title: '申报单位', field: 'logname', align: 'center', valign: 'middle',width:'5%'},
        {title: '操作员', field: 'account', align: 'center', valign: 'middle',width:'5%'},
        {title: '收发货人', field: 'userName', align: 'center', valign: 'middle',width:'5%'},
        {title: '统一编号 | 海关编号', field: 'createtime', align: 'center', valign: 'middle',width:'10%'},
        {title: '提运单号', field: 'message', align: 'center', valign: 'middle',width:'5%'},
        {title: '出口口岸', field: 'ip', align: 'center', valign: 'middle',width:'5%'},
        {title: '业务状态', field: 'ip', align: 'center', valign: 'middle',width:'5%'},
        {title: '上传状态', field: 'ip', align: 'center', valign: 'middle',width:'5%'},
        {
            title: '操作', visible: true, align: 'center', valign: 'middle', formatter: function (value, row, index) {
            return '<button type="button" class="btn btn-primary button-margin" onclick="Expense.findRecord(' + row.id + ')" id=""><i class="fa fa-eye"></i>&nbsp;查看</button>' +
                '<button type="button" class="btn btn-danger button-margin" onclick="Expense.deleteRecord(' + row.id + ')" id=""><i class="fa fa-edit"></i>&nbsp;修改</button>';
            }
        }];
};

/**
 * 检查是否选中
 */
customs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        customs.seItem = selected[0];
        return true;
    }
};


/**
 * 查看日志详情
 */
customs.detail = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/log/detail/" + this.seItem.id, function (data) {
            Feng.infoDetail("日志详情", data.regularMessage);
        }, function (data) {
            Feng.error("获取详情失败!");
        });
        ajax.start();
    }
};


/**
 * 清空日志
 */
customs.delLog = function () {
    Feng.confirm("是否清空所有日志?", function () {
        var ajax = Feng.baseAjax("/loginLog/delLoginLog", "清空日志");
        ajax.start();
        LoginLog.table.refresh();
    });
}

/**
 * 查询日志列表
 */
customs.search = function () {
    var queryData = {};

    queryData['logName'] = $("#logName").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();

    customs.table.refresh({query: queryData});
};

$(function () {

    init();

    var defaultColunms = customs.initColumn();
    var table = new BSTable(customs.id, "/loginLog/list", defaultColunms);
    table.setPaginationType("server");
    customs.table = table.init();
});

function init() {

    var BootstrapTable = $.fn.bootstrapTable.Constructor;
    BootstrapTable.prototype.onSort = function (event) {
        var $this = event.type === "keypress" ? $(event.currentTarget) : $(event.currentTarget).parent(),
            $this_ = this.$header.find('th').eq($this.index()),
            sortName = this.header.sortNames[$this.index()];

        this.$header.add(this.$header_).find('span.order').remove();

        if (this.options.sortName === $this.data('field')) {
            this.options.sortOrder = this.options.sortOrder === 'asc' ? 'desc' : 'asc';
        } else {
            this.options.sortName = sortName || $this.data('field');
            this.options.sortOrder = $this.data('order') === 'asc' ? 'desc' : 'asc';
        }
        this.trigger('sort', this.options.sortName, this.options.sortOrder);

        $this.add($this_).data('order', this.options.sortOrder);

        // Assign the correct sortable arrow
        this.getCaret();

        if (this.options.sidePagination === 'server') {
            this.initServer(this.options.silentSort);
            return;
        }

        this.initSort();
        this.initBody();
    };
    BootstrapTable.prototype.getCaret = function () {
        var that = this;

        $.each(this.$header.find('th'), function (i, th) {
            var sortName = that.header.sortNames[i];
            $(th).find('.sortable').removeClass('desc asc').addClass((sortName || $(th).data('field')) === that.options.sortName ? that.options.sortOrder : 'both');
        });
    };
}
