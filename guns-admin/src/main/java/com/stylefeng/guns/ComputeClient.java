package com.stylefeng.guns;

import com.stylefeng.guns.controller.ComputeClientHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 引用service
 */
//@FeignClient(value = "compute-service", fallback = ComputeClientHystrix.class)
public interface ComputeClient {

    /**
     *
     * @param a
     * @param b
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/add")
    Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b);

    /**
     * 发邮件
     * @param sendTo
     * @param titel
     * @param content
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/getTestDemoEmail")
    String sendSimpleMail(@RequestParam(value = "sendTo") String sendTo, @RequestParam(value = "titel") String titel, @RequestParam(value = "content") String content);

    /**
     *
     * @param
     * @param title
     * @param templateName
     * @param
     */
    @RequestMapping(method = RequestMethod.GET, value = "/sendMessage")
    void sendMessageMail(@RequestParam(value = "messageCode") String messageCode, @RequestParam(value = "messageStatus") String messageStatus, @RequestParam(value = "cause") String cause, @RequestParam(value = "title") String title, @RequestParam(value = "templateName") String templateName, @RequestParam(value = "sendTo") String sendTo);

}
