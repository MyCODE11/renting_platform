package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 合同信息
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
public class Compact extends Model<Compact> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 对应的房屋对象的ID
     */
    @TableField("room_id")
    private Integer roomId;
    /**
     * 合同编号
     */
    @TableField("compact_number")
    private String compactNumber;
    /**
     * 合同开始时间
     */
    @TableField("start_date")
    private Date startDate;
    /**
     * 合同结束时间
     */
    @TableField("end_date")
    private Date endDate;
    /**
     * 租赁周期，/天
     */
    @TableField("rent_cycle")
    private Integer rentCycle;
    /**
     * 收费周期，/月
     */
    @TableField("rent_expense_cycle")
    private Integer rentExpenseCycle;
    /**
     * 对应的经纪人的ID
     */
    @TableField("broker_id")
    private Integer brokerId;
    /**
     * 经营用途
     */
    private String manage;
    /**
     * 合同附件地址，多个附件使用分号隔开；
     */
    @TableField("compact_image")
    private String compactImage;
    /**
     * 合同备注信息
     */
    private String rembark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getCompactNumber() {
        return compactNumber;
    }

    public void setCompactNumber(String compactNumber) {
        this.compactNumber = compactNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getRentCycle() {
        return rentCycle;
    }

    public void setRentCycle(Integer rentCycle) {
        this.rentCycle = rentCycle;
    }

    public Integer getRentExpenseCycle() {
        return rentExpenseCycle;
    }

    public void setRentExpenseCycle(Integer rentExpenseCycle) {
        this.rentExpenseCycle = rentExpenseCycle;
    }

    public Integer getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(Integer brokerId) {
        this.brokerId = brokerId;
    }

    public String getManage() {
        return manage;
    }

    public void setManage(String manage) {
        this.manage = manage;
    }

    public String getCompactImage() {
        return compactImage;
    }

    public void setCompactImage(String compactImage) {
        this.compactImage = compactImage;
    }

    public String getRembark() {
        return rembark;
    }

    public void setRembark(String rembark) {
        this.rembark = rembark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Compact{" +
        "id=" + id +
        ", roomId=" + roomId +
        ", compactNumber=" + compactNumber +
        ", startDate=" + startDate +
        ", endDate=" + endDate +
        ", rentCycle=" + rentCycle +
        ", rentExpenseCycle=" + rentExpenseCycle +
        ", brokerId=" + brokerId +
        ", manage=" + manage +
        ", compactImage=" + compactImage +
        ", rembark=" + rembark +
        "}";
    }
}
