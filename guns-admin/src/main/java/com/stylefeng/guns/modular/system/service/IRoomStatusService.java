package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.RoomStatus;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间状态表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
public interface IRoomStatusService extends IService<RoomStatus> {

}
