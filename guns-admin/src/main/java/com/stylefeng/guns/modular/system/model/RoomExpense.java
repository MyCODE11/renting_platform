package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间费用
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@TableName("room_expense")
public class RoomExpense extends Model<RoomExpense> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 对应的房间对象的ID
     */
    @TableField("room_id")
    private Integer roomId;
    /**
     * 费用名称
     */
    private String name;
    /**
     * 费用计价方式：1-按房间一次性收费；2-按房间每月收费；3-按房间每日收费；4-抄表各房间独立结算；5-其他类型；
     */
    @TableField("valuation_type")
    private Integer valuationType;
    /**
     * 费用单价
     */
    private BigDecimal price;
    /**
     * 费用计量单位
     */
    private String unit;
    /**
     * 费用备注
     */
    private String remark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValuationType() {
        return valuationType;
    }

    public void setValuationType(Integer valuationType) {
        this.valuationType = valuationType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RoomExpense{" +
        "id=" + id +
        ", roomId=" + roomId +
        ", name=" + name +
        ", valuationType=" + valuationType +
        ", price=" + price +
        ", unit=" + unit +
        ", remark=" + remark +
        "}";
    }
}
