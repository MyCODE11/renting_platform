package com.stylefeng.guns.modular.system.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.system.dao.*;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.model.DTO.RoomCountDTO;
import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;
import com.stylefeng.guns.modular.system.service.IRoomService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.system.utils.DateUtils;
import com.stylefeng.guns.modular.system.utils.ListUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 房间对象 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-09
 */
@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements IRoomService {
    @Resource
    private RoomMapper roomMapper;
    @Resource
    private RoomExpenseMapper roomExpenseMapper;
    @Resource
    private RoomPropertyMapper roomPropertyMapper;
    @Resource
    private CompactMapper compactMapper;
    @Resource
    private RoomStatusMapper roomStatusMapper;
    @Resource
    private PaymentRecordMapper paymentRecordMapper;

    /**
     * 获取某年每月的租房统计量
     *
     * @param params
     * @return
     */
    public List<RoomCountDTO> selectRoomCountEveryMonth(Map<String, Object> params) {
        Date currentDate = new Date();
        //获取12个月
        long currentDateTime = currentDate.getTime();
        //获取时间参数
        String dateTypeStr = params.get("dateType").toString();
        List<Date> months = null;
        if ("0".equals(dateTypeStr)) {
            //当年
            params.put("date", currentDate);
            months = DateUtils.get12Months(currentDateTime);
        } else {
            //去年
            Date dateLastYear = DateUtils.getDateLastYear(currentDate);
            params.put("date", dateLastYear);
            //获取去年12个月
            months = DateUtils.get12Months(dateLastYear.getTime());
        }

        List<RoomCountDTO> roomCountDTOS = roomMapper.selectRoomCountEveryMonth(params);
        //遍历12个月，补漏
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        for (Date date : months) {
            String dateStr = sdf.format(date);
            Boolean flag = false;
            for (RoomCountDTO roomCountDTO : roomCountDTOS) {
                String month = roomCountDTO.getMonth();
                if (dateStr.equals(month)) {
                    flag = true;
                }
            }

            if (!flag) {
                //进入此处说明此月份没有数据
                RoomCountDTO roomCountDTO = new RoomCountDTO();
                roomCountDTO.setCount(0);
                roomCountDTO.setMonth(dateStr);
                roomCountDTOS.add(roomCountDTO);
            }
        }
        //对集合进行排序
        ListUtils.sortByStrDate(roomCountDTOS);
        return roomCountDTOS;
    }


    /**
     * 获取总房量(装修后的总房量)
     */
    public Integer getRoomCount() {
        EntityWrapper entityWrapper = new EntityWrapper(new Room());
        entityWrapper.isNotNull("id");
        entityWrapper.isNull("room_kind");
        Integer count = roomMapper.selectCount(entityWrapper);
        return count;
    }

    /**
     * 查询未出租或者已出租的房量0-未出租，1-已出租
     *
     * @param params
     * @return
     */
    public Integer selectRoomCountIsRent(Map<String, Object> params) {
        Integer integer = roomMapper.selectRoomCountIsRent(params);
        return integer;
    }

    /**
     * 删除房源
     */
    public void deleteRoomById(Integer roomId) {
        //删除房间信息
        roomMapper.deleteById(roomId);
        //删除房间费用信息
        roomExpenseMapper.deleteByRoomId(roomId);
        //删除房间资产信息
        roomPropertyMapper.deleteByRoomId(roomId);

        //删除房间缴费记录
        Wrapper wrapper = new EntityWrapper(new PaymentRecord());
        wrapper.where("room_id = {0}", roomId);
        paymentRecordMapper.delete(wrapper);

        //删除合同信息
        EntityWrapper entityWrapper = new EntityWrapper(new Compact());
        entityWrapper.where("room_id = {0}", roomId);
        compactMapper.delete(entityWrapper);

        //删除房屋状态信息
        entityWrapper.setEntity(new RoomStatus());
        roomStatusMapper.delete(entityWrapper);

    }

    /**
     * 根据房间ID查询房间对象
     *
     * @return
     */
    public RoomDTO selectRoomByRoomId(Integer roomId) {
        Map<String, Object> params = new HashMap<>();
        params.put("roomId", roomId);

        List<RoomDTO> roomDTOS = roomMapper.selectRoomForManage(params);

        if (roomDTOS != null && roomDTOS.size() > 0) {
            return roomDTOS.get(0);
        } else {
            return null;
        }
    }

    /**
     * 用于房源管理页面的房间对象查询
     *
     * @return
     */
    public List<RoomDTO> selectRoomForManage(Map<String, Object> params) {

        List<RoomDTO> roomDTOS = roomMapper.selectRoomForManage(params);

        return roomDTOS;
    }
}
