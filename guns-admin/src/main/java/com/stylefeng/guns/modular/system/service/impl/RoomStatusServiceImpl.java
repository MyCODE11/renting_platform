package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.RoomStatus;
import com.stylefeng.guns.modular.system.dao.RoomStatusMapper;
import com.stylefeng.guns.modular.system.service.IRoomStatusService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间状态表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
@Service
public class RoomStatusServiceImpl extends ServiceImpl<RoomStatusMapper, RoomStatus> implements IRoomStatusService {

}
