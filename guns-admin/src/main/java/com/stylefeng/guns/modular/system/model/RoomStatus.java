package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间状态表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
@TableName("room_status")
public class RoomStatus extends Model<RoomStatus> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 对应的房间对象的ID
     */
    @TableField("room_id")
    private Integer roomId;
    /**
     * 租客人数
     */
    @TableField("renter_number")
    private Integer renterNumber;
    /**
     * 是否入住/出租0-否；1-是；
     */
    @TableField("is_rent")
    private Integer isRent;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getRenterNumber() {
        return renterNumber;
    }

    public void setRenterNumber(Integer renterNumber) {
        this.renterNumber = renterNumber;
    }

    public Integer getIsRent() {
        return isRent;
    }

    public void setIsRent(Integer isRent) {
        this.isRent = isRent;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RoomStatus{" +
        "id=" + id +
        ", roomId=" + roomId +
        ", renterNumber=" + renterNumber +
        ", isRent=" + isRent +
        "}";
    }
}
