package com.stylefeng.guns.modular.system.utils;

import com.stylefeng.guns.modular.system.model.DTO.RoomCountDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ListUtils {
    //根据对象中的字符串类型的日期属性进行排序
    public static void sortByStrDate(List<RoomCountDTO> list) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Collections.sort(list, new Comparator<RoomCountDTO>() {

            @Override
            public int compare(RoomCountDTO o1, RoomCountDTO o2) {
                String month1 = o1.getMonth();
                String month2 = o2.getMonth();
                int i = 0;
                try {
                    Date parse1 = sdf.parse(month1);
                    Date parse2 = sdf.parse(month2);
                    long l = parse1.getTime() - parse2.getTime();
                    if (l > 0) {
                        i = 1;
                    } else {
                        i = -1;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return i;
            }
        });
    }

}
