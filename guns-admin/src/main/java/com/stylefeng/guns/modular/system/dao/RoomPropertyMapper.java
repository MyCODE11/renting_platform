package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.system.model.DTO.RoomPropertyDTOWithNumber;
import com.stylefeng.guns.modular.system.model.RoomProperty;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；） Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-04
 */
@Repository
public interface RoomPropertyMapper extends BaseMapper<RoomProperty> {
    /**
     * 根据房间ID查询房间资产对象
     *
     * @param roomId
     * @return
     */
    List<RoomPropertyDTOWithNumber> selectByRoomId(Integer roomId);

    /**
     * 根据房间对象ID删除房间资产信息
     *
     * @param roomId
     * @return
     */
    int deleteByRoomId(Integer roomId);
}
