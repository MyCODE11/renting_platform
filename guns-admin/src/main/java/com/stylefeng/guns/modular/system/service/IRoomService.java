package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.DTO.RoomCountDTO;
import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;
import com.stylefeng.guns.modular.system.model.Room;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间对象 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-09
 */
public interface IRoomService extends IService<Room> {
    /**
     * 获取某年每月的租房统计量(只有当月有数据才返回数据)
     *
     * @param params
     * @return
     */
    public List<RoomCountDTO> selectRoomCountEveryMonth(Map<String, Object> params);

    /**
     * 根据房间ID查询房间对象
     *
     * @return
     */
    public RoomDTO selectRoomByRoomId(Integer roomId);

    /**
     * 查询未出租或者已出租的房量0-未出租，1-已出租
     *
     * @param params
     * @return
     */
    public Integer selectRoomCountIsRent(Map<String, Object> params);

    /**
     * 获取总房量
     */
    public Integer getRoomCount();

    /**
     * 删除房源
     */
    public void deleteRoomById(Integer roomId);

    /**
     * 用于房源管理页面的信息查询
     *
     * @param params
     * @return
     */
    public List<RoomDTO> selectRoomForManage(Map<String, Object> params);

}
