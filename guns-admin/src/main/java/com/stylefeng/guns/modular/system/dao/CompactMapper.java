package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Compact;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 合同信息 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
@Repository
public interface CompactMapper extends BaseMapper<Compact> {

}
