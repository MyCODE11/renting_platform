package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.PaymentRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 该表用于表示每个房间的缴费记录 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-25
 */
@Repository
public interface PaymentRecordMapper extends BaseMapper<PaymentRecord> {

}
