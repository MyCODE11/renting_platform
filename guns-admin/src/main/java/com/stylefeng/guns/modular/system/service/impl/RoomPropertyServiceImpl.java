package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.DTO.RoomPropertyDTO;
import com.stylefeng.guns.modular.system.model.DTO.RoomPropertyDTOWithNumber;
import com.stylefeng.guns.modular.system.model.RoomProperty;
import com.stylefeng.guns.modular.system.dao.RoomPropertyMapper;
import com.stylefeng.guns.modular.system.service.IRoomPropertyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；） 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-04
 */
@Service
public class RoomPropertyServiceImpl extends ServiceImpl<RoomPropertyMapper, RoomProperty> implements IRoomPropertyService {
    @Resource
    private RoomPropertyMapper roomPropertyMapper;

    /**
     * 根据房间ID查询房间资产对象
     *
     * @param roomId
     * @return
     */
    public List<RoomPropertyDTOWithNumber> selectByRoomId(Integer roomId) {
        return roomPropertyMapper.selectByRoomId(roomId);
    }

}
