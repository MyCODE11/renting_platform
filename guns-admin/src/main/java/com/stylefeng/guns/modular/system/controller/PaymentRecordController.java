package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.system.model.Room;
import com.stylefeng.guns.modular.system.service.IRoomService;
import com.stylefeng.guns.modular.system.utils.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.PaymentRecord;
import com.stylefeng.guns.modular.system.service.IPaymentRecordService;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 控制器
 *
 * @author kai_zhang
 * @Date 2018-05-25 16:46:48
 */
@Controller
@RequestMapping("/paymentRecord")
public class PaymentRecordController extends BaseController {

    @Autowired
    private IPaymentRecordService paymentRecordService;
    @Resource
    private IRoomService roomServiceImpl;


    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return paymentRecordService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PaymentRecord paymentRecord) {
        //获取缴费月数,根据缴费月数修改房间的到期时间
        Integer count = paymentRecord.getCount();
        //根据房间ID查询房间对象
        Integer roomId = paymentRecord.getRoomId();
        Room room = roomServiceImpl.selectById(roomId);
        //修改房间的到期时间
        Date endDate = room.getEndDate();
        Date dateAfterMonth = DateUtils.getDateAfterMonth(endDate, count);
        room.setEndDate(dateAfterMonth);
        //更新房间的到期时间
        roomServiceImpl.updateById(room);


        //保存缴费记录
        paymentRecordService.insert(paymentRecord);

        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer paymentRecordId) {
        paymentRecordService.deleteById(paymentRecordId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PaymentRecord paymentRecord) {
        paymentRecordService.updateById(paymentRecord);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{paymentRecordId}")
    @ResponseBody
    public Object detail(@PathVariable("paymentRecordId") Integer paymentRecordId) {
        return paymentRecordService.selectById(paymentRecordId);
    }
}
