package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.Compact;
import com.stylefeng.guns.modular.system.dao.CompactMapper;
import com.stylefeng.guns.modular.system.service.ICompactService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 合同信息 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
@Service
public class CompactServiceImpl extends ServiceImpl<CompactMapper, Compact> implements ICompactService {

}
