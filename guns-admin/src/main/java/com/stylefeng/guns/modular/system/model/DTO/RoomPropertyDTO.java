package com.stylefeng.guns.modular.system.model.DTO;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

public class RoomPropertyDTO {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 房间资产名称
     */
    private String name;

    /**
     * 资产状态码
     */
    private Integer status;

    @Override
    public String toString() {
        return "RoomPropertyDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
