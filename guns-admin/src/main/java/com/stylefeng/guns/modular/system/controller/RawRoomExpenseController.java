package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.modular.system.service.IRawRoomExpenseService;

/**
 * 控制器
 *
 * @author kai_zhang
 * @Date 2018-05-28 09:25:40
 */
@Controller
@RequestMapping("/rawRoomExpense")
public class RawRoomExpenseController extends BaseController {

    private String PREFIX = "/system/rawRoomExpense/";

    @Autowired
    private IRawRoomExpenseService rawRoomExpenseService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "rawRoomExpense.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/rawRoomExpense_add")
    public String rawRoomExpenseAdd() {
        return PREFIX + "rawRoomExpense_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/rawRoomExpense_update/{rawRoomExpenseId}")
    public String rawRoomExpenseUpdate(@PathVariable Integer rawRoomExpenseId, Model model) {
//        RawRoomExpense rawRoomExpense = rawRoomExpenseService.selectById(rawRoomExpenseId);
//        model.addAttribute("item",rawRoomExpense);
//        LogObjectHolder.me().set(rawRoomExpense);
        return PREFIX + "rawRoomExpense_edit.html";
    }

}
