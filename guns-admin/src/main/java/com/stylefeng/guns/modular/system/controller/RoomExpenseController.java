package com.stylefeng.guns.modular.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.system.model.RoomExpense;
import com.stylefeng.guns.modular.system.service.IRoomExpenseService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 控制器
 *
 * @author kai_zhang
 * @Date 2018-05-04 14:49:34
 */
@Controller
@RequestMapping("/roomExpense")
public class RoomExpenseController extends BaseController {

    private String PREFIX = "/system/roomExpense/";

    @Autowired
    private IRoomExpenseService roomExpenseService;

    /**
     * 根据费用ID获取费用对象
     */
    @RequestMapping(value = "/selectExpenseById",method = RequestMethod.GET)
    @ResponseBody
    public Object selectExpenseById(@Param("id") Integer id) {
        RoomExpense roomExpense = roomExpenseService.selectById(id);
        return roomExpense;
    }


    /**
     * 根据roomId,获取费用
     */
    @RequestMapping("/selectExpenseByRoomId")
    @ResponseBody
    public Object selectExpenseByRoomId(@PathVariable Integer roomId) {
        //根据Roomid返回费用集合
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.setEntity(new RoomExpense());
        entityWrapper.where("room_id = {0}", roomId);
        List roomExpenseList = roomExpenseService.selectList(entityWrapper);

        return roomExpenseList;
    }

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "roomExpense.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/roomExpense_add")
    public String roomExpenseAdd() {
        return PREFIX + "roomExpense_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/roomExpense_update/{roomExpenseId}")
    public String roomExpenseUpdate(@PathVariable Integer roomExpenseId, Model model) {
        RoomExpense roomExpense = roomExpenseService.selectById(roomExpenseId);
        model.addAttribute("item", roomExpense);
        LogObjectHolder.me().set(roomExpense);
        return PREFIX + "roomExpense_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return roomExpenseService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(RoomExpense roomExpense) {
        roomExpenseService.insert(roomExpense);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer roomExpenseId) {
        roomExpenseService.deleteById(roomExpenseId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(RoomExpense roomExpense) {
        roomExpenseService.updateById(roomExpense);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{roomExpenseId}")
    @ResponseBody
    public Object detail(@PathVariable("roomExpenseId") Integer roomExpenseId) {
        return roomExpenseService.selectById(roomExpenseId);
    }
}
