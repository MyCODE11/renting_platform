package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.RoomExpense;
import com.stylefeng.guns.modular.system.dao.RoomExpenseMapper;
import com.stylefeng.guns.modular.system.service.IRoomExpenseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间费用 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Service
public class RoomExpenseServiceImpl extends ServiceImpl<RoomExpenseMapper, RoomExpense> implements IRoomExpenseService {

}
