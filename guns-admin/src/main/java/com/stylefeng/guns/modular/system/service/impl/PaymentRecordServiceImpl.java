package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.PaymentRecord;
import com.stylefeng.guns.modular.system.dao.PaymentRecordMapper;
import com.stylefeng.guns.modular.system.service.IPaymentRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 该表用于表示每个房间的缴费记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-25
 */
@Service
public class PaymentRecordServiceImpl extends ServiceImpl<PaymentRecordMapper, PaymentRecord> implements IPaymentRecordService {

}
