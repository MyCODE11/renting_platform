package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.RoomStatus;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 房间状态表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
@Repository
public interface RoomStatusMapper extends BaseMapper<RoomStatus> {

}
