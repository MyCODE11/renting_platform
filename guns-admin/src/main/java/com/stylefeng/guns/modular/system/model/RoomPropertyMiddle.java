package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；）和房间对象的中间表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-07
 */
@TableName("room_property_middle")
public class RoomPropertyMiddle extends Model<RoomPropertyMiddle> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 对应的房间对象的ID
     */
    @TableField("room_id")
    private Integer roomId;
    /**
     * 对应的房间资产对象的ID
     */
    @TableField("property_id")
    private Integer propertyId;
    /**
     * 资产数量
     */
    @TableField("number")
    private Integer number;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RoomPropertyMiddle{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", propertyId=" + propertyId +
                ", number=" + number +
                '}';
    }
}
