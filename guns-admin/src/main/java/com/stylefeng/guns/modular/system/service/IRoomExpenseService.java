package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.RoomExpense;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间费用 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
public interface IRoomExpenseService extends IService<RoomExpense> {

}
