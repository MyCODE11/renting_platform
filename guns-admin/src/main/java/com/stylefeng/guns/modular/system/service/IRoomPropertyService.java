package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.DTO.RoomPropertyDTO;
import com.stylefeng.guns.modular.system.model.DTO.RoomPropertyDTOWithNumber;
import com.stylefeng.guns.modular.system.model.RoomProperty;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；） 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-04
 */
public interface IRoomPropertyService extends IService<RoomProperty> {
    /**
     * 根据房间ID查询房间资产对象
     *
     * @param roomId
     * @return
     */
    List<RoomPropertyDTOWithNumber> selectByRoomId(Integer roomId);

}
