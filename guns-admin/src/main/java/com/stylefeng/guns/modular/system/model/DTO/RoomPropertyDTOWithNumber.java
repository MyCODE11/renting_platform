package com.stylefeng.guns.modular.system.model.DTO;

/**
 * 用于添加资产时添加资产数量的自定义类
 */
public class RoomPropertyDTOWithNumber {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 房间资产名称
     */
    private String name;

    /**
     * 资产数量
     */
    private Integer number;

    @Override
    public String toString() {
        return "RoomPropertyDTOWithNumber{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number=" + number +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
