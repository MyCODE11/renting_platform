package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.RoomPropertyMiddle;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；）和房间对象的中间表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-07
 */
public interface IRoomPropertyMiddleService extends IService<RoomPropertyMiddle> {

}
