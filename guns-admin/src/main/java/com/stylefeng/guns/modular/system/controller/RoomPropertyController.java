package com.stylefeng.guns.modular.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;
import com.stylefeng.guns.modular.system.model.PaymentRecord;
import com.stylefeng.guns.modular.system.model.RoomExpense;
import com.stylefeng.guns.modular.system.model.User;
import com.stylefeng.guns.modular.system.service.IPaymentRecordService;
import com.stylefeng.guns.modular.system.service.IRoomService;
import com.stylefeng.guns.modular.system.service.IUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.system.model.RoomProperty;
import com.stylefeng.guns.modular.system.service.IRoomPropertyService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制器
 *
 * @author kai_zhang
 * @Date 2018-05-04 14:49:43
 */
@Controller
@RequestMapping("/roomProperty")
public class RoomPropertyController extends BaseController {

    private String PREFIX = "/system/roomProperty/";

    @Autowired
    private IRoomPropertyService roomPropertyService;
    @Resource
    private IRoomService roomService;
    @Resource
    private IUserService userService;
    @Resource
    private IPaymentRecordService paymentRecordServiceImpl;

    /**
     * 获取所有的资产
     */
    @RequestMapping("/selectAllProperty")
    @ResponseBody
    public Object selectAllProperty() {
        List<RoomProperty> propertyList = roomPropertyService.selectList(null);
        return propertyList;
    }

    /**
     * 根据ID，查询指定的房间对象
     */
    @RequestMapping(value = "/selectRoomByRoomId", method = RequestMethod.GET)
    public String selectRoomByRoomId(@Param("roomId") Integer roomId, HttpServletResponse response) {
        //根据房间ID查询缴费记录
        EntityWrapper entityWrapper = new EntityWrapper(new PaymentRecord());
        entityWrapper.where("room_id = {0}", roomId);
        List list = paymentRecordServiceImpl.selectList(entityWrapper);


        RoomDTO roomDTO = roomService.selectRoomByRoomId(roomId);
        roomDTO.setRecordList(list);
        try {
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().println(JSONObject.toJSON(roomDTO));
        } catch (IOException e) {
            e.printStackTrace();
        }
        LogObjectHolder.me().set(roomDTO);
        return PREFIX + "roomProperty.html";
    }

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(String condition, Model model, HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        if (condition != null) {
            //前台传递了模糊查询条件；
            params.put("condition", condition);
        }

        //获取用户信息
        ShiroUser shiroUser = (ShiroUser) request.getSession().getAttribute("shiroUser");
        Integer userId = shiroUser.getId();
        User user = this.userService.selectById(userId);
        String roleid = user.getRoleid();

        //判断，如果未超级管理员，查询所有房源，否则只查询对应操作员的房源；
        List<RoomDTO> roomDTOS = null;
        if ("1".equals(roleid)) {
            //查询所有
            roomDTOS = roomService.selectRoomForManage(params);
        } else {
            //查询特定管理员的房源信息
            params.put("userId", userId);
            roomDTOS = roomService.selectRoomForManage(params);
        }

        model.addAttribute("item", roomDTOS);
        LogObjectHolder.me().set(roomDTOS);
        return PREFIX + "roomProperty.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/roomProperty_add")
    public String roomPropertyAdd() {
        return PREFIX + "roomProperty_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/roomProperty_update/{roomPropertyId}")
    public String roomPropertyUpdate(@PathVariable Integer roomPropertyId, Model model) {
        RoomProperty roomProperty = roomPropertyService.selectById(roomPropertyId);
        model.addAttribute("item", roomProperty);
        LogObjectHolder.me().set(roomProperty);
        return PREFIX + "roomProperty_edit.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/roomProperty")
    public String roomProperty(Model model) {
        EntityWrapper entityWrapper = new EntityWrapper(new RoomProperty());
        entityWrapper.where("id is not null");
        List list = roomPropertyService.selectList(entityWrapper);
        model.addAttribute("item", list);
        LogObjectHolder.me().set(list);
        return PREFIX + "roomProperty.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return roomPropertyService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(RoomProperty roomProperty) {
        roomPropertyService.insert(roomProperty);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer roomPropertyId) {
        roomPropertyService.deleteById(roomPropertyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(RoomProperty roomProperty) {
        roomPropertyService.updateById(roomProperty);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{roomPropertyId}")
    @ResponseBody
    public Object detail(@PathVariable("roomPropertyId") Integer roomPropertyId) {
        return roomPropertyService.selectById(roomPropertyId);
    }
}
