package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.service.IRawRoomExpenseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 毛坯房房间费用 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-28
 */
@Service
public class RawRoomExpenseServiceImpl implements IRawRoomExpenseService {

}
