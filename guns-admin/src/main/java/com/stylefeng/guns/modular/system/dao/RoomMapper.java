package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.DTO.RoomCountDTO;
import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;
import com.stylefeng.guns.modular.system.model.Room;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间对象 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-09
 */
@Repository
public interface RoomMapper extends BaseMapper<Room> {
    /**
     * 某年每月房量统计
     *
     * @param params
     * @return
     */
    List<RoomCountDTO> selectRoomCountEveryMonth(Map<String, Object> params);

    /**
     * 查询未出租或者已出租的房量0-未出租，1-已出租
     *
     * @param params
     * @return
     */
    Integer selectRoomCountIsRent(Map<String, Object> params);

    /**
     * 用于房源管理页面的房间对象查询
     *
     * @return
     */
    List<RoomDTO> selectRoomForManage(Map<String, Object> params);

    /**
     * 用于毛坯房房源管理页面的房间对象查询
     *
     * @return
     */
    List<RoomDTO> selectRawRoomForManage(Map<String, Object> params);

}
