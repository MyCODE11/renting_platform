package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 房间对象
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-09
 */
public class Room extends Model<Room> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 分店名称
     */
    private String name;
    /**
     * 楼宇编码
     */
    @TableField("build_code")
    private String buildCode;
    /**
     * 楼层
     */
    private String floor;
    /**
     * 房号
     */
    @TableField("room_number")
    private String roomNumber;
    /**
     * 房间面积
     */
    @TableField("room_area")
    private Double roomArea;
    /**
     * 排序号
     */
    @TableField("sort_number")
    private Integer sortNumber;
    /**
     * 房间图片,多张图片使用“;”隔开
     */
    @TableField("room_image")
    private String roomImage;
    /**
     * 房间户型：
     */
    @TableField("room_type")
    private String roomType;
    /**
     * 装修类型
     */
    @TableField("decoration_type")
    private String decorationType;
    /**
     * 用途
     */
    @TableField("use_type")
    private String useType;
    /**
     * 出租类型:整租，合租
     */
    @TableField("rent_type")
    private String rentType;
    /**
     * 操作员ID
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 起租日期
     */
    @TableField("start_date")
    private Date startDate;
    /**
     * 租赁结束日期
     */
    @TableField("end_date")
    private Date endDate;

    /**
     * 操作员ID
     */
    @TableField("room_kind")
    private Integer roomKind;

    public Integer getRoomKind() {
        return roomKind;
    }

    public void setRoomKind(Integer roomKind) {
        this.roomKind = roomKind;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuildCode() {
        return buildCode;
    }

    public void setBuildCode(String buildCode) {
        this.buildCode = buildCode;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Double getRoomArea() {
        return roomArea;
    }

    public void setRoomArea(Double roomArea) {
        this.roomArea = roomArea;
    }

    public Integer getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(Integer sortNumber) {
        this.sortNumber = sortNumber;
    }

    public String getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(String roomImage) {
        this.roomImage = roomImage;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getDecorationType() {
        return decorationType;
    }

    public void setDecorationType(String decorationType) {
        this.decorationType = decorationType;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getRentType() {
        return rentType;
    }

    public void setRentType(String rentType) {
        this.rentType = rentType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", buildCode='" + buildCode + '\'' +
                ", floor='" + floor + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", roomArea=" + roomArea +
                ", sortNumber=" + sortNumber +
                ", roomImage='" + roomImage + '\'' +
                ", roomType='" + roomType + '\'' +
                ", decorationType='" + decorationType + '\'' +
                ", useType='" + useType + '\'' +
                ", rentType='" + rentType + '\'' +
                ", userId=" + userId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", roomKind=" + roomKind +
                '}';
    }
}
