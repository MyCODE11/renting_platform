package com.stylefeng.guns.modular.system.model.DTO;

import com.baomidou.mybatisplus.annotations.TableField;
import com.stylefeng.guns.modular.system.model.PaymentRecord;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用于房源管理页面房屋对象的自定义对象
 */
public class RoomDTO {
    //id
    private Integer id;
    //名称
    private String name;
    //房间编码
    private String buildCode;
    //房间用途
    private String useType;
    //房间类型
    private String roomType;
    //房间面积
    private Double roomArea;
    //房间图片
    private String roomImage;
    //月租金
    private BigDecimal monthRent;
    //付款方式
    private String valuationType;
    //押金
    private BigDecimal pledgeCash;
    //费用清单
    private String expenseStr;
    //是否入住
    private String is_rent;
    //经纪人
    private String broker;
    /**
     * 起租日期
     */
    private Date startDate;
    /**
     * 租赁结束日期
     */
    private Date endDate;

    private List<PaymentRecord> recordList = new ArrayList<>();

    public String getValuationType() {
        return valuationType;
    }

    public void setValuationType(String valuationType) {
        this.valuationType = valuationType;
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", buildCode='" + buildCode + '\'' +
                ", useType='" + useType + '\'' +
                ", roomType='" + roomType + '\'' +
                ", roomArea=" + roomArea +
                ", roomImage='" + roomImage + '\'' +
                ", monthRent=" + monthRent +
                ", valuationType=" + valuationType +
                ", pledgeCash=" + pledgeCash +
                ", expenseStr='" + expenseStr + '\'' +
                ", is_rent='" + is_rent + '\'' +
                ", broker='" + broker + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", recordList=" + recordList +
                '}';
    }

    public List<PaymentRecord> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<PaymentRecord> recordList) {
        this.recordList = recordList;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuildCode() {
        return buildCode;
    }

    public void setBuildCode(String buildCode) {
        this.buildCode = buildCode;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Double getRoomArea() {
        return roomArea;
    }

    public void setRoomArea(Double roomArea) {
        this.roomArea = roomArea;
    }

    public String getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(String roomImage) {
        this.roomImage = roomImage;
    }

    public BigDecimal getMonthRent() {
        return monthRent;
    }

    public void setMonthRent(BigDecimal monthRent) {
        this.monthRent = monthRent;
    }

    public BigDecimal getPledgeCash() {
        return pledgeCash;
    }

    public void setPledgeCash(BigDecimal pledgeCash) {
        this.pledgeCash = pledgeCash;
    }

    public String getExpenseStr() {
        return expenseStr;
    }

    public void setExpenseStr(String expenseStr) {
        this.expenseStr = expenseStr;
    }

    public String getIs_rent() {
        return is_rent;
    }

    public void setIs_rent(String is_rent) {
        this.is_rent = is_rent;
    }

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }
}
