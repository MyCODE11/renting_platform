package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.dao.RoomExpenseMapper;
import com.stylefeng.guns.modular.system.dao.RoomMapper;
import com.stylefeng.guns.modular.system.dao.RoomPropertyMapper;
import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;
import com.stylefeng.guns.modular.system.service.IRawRoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 毛坯房房间对象 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-28
 */
@Service
public class RawRoomServiceImpl implements IRawRoomService {
    @Resource
    private RoomMapper roomMapper;
    @Resource
    private RoomExpenseMapper roomExpenseMapper;
    @Resource
    private RoomPropertyMapper roomPropertyMapper;

    /**
     * 删除房源
     */
    public void deleteRoomById(Integer roomId) {
        //删除房间信息
        roomMapper.deleteById(roomId);
        //删除房间费用信息
        roomExpenseMapper.deleteByRoomId(roomId);
        //删除房间资产信息
        roomPropertyMapper.deleteByRoomId(roomId);
    }

    /**
     * 用于毛坯房房源管理页面的房间对象查询
     *
     * @return
     */
    public List<RoomDTO> selectRawRoomForManage(Map<String, Object> params) {

        List<RoomDTO> roomDTOS = roomMapper.selectRawRoomForManage(params);

        return roomDTOS;
    }

}
