package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.RoomExpense;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 房间费用 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Repository
public interface RoomExpenseMapper extends BaseMapper<RoomExpense> {
    /**
     * 根据房间对象ID删除房间费用信息
     *
     * @param roomId
     * @return
     */
    int deleteByRoomId(Integer roomId);

}
