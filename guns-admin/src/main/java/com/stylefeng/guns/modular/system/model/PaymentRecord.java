package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 该表用于表示每个房间的缴费记录
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-25
 */
@TableName("payment_record")
public class PaymentRecord extends Model<PaymentRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 缴费记录对应的房间ID
     */
    @TableField("room_id")
    private Integer roomId;
    /**
     * 费用名称
     */
    @TableField("expense_name")
    private String expenseName;
    /**
     * 缴费金额
     */
    private BigDecimal amount;
    /**
     * 缴费月数
     */
    private Integer count;
    /**
     * 缴费时间
     */
    @TableField("pay_time")
    private Date payTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PaymentRecord{" +
        "id=" + id +
        ", roomId=" + roomId +
        ", expenseName=" + expenseName +
        ", amount=" + amount +
        ", count=" + count +
        ", payTime=" + payTime +
        "}";
    }
}
