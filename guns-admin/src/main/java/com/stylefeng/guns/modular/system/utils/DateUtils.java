package com.stylefeng.guns.modular.system.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 时间工具类
 */
public class DateUtils {
    /**
     * 获取去年此时此刻
     */
    public static Date getDateLastYear(Date currentDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.YEAR, -1);

        Date lastYearDate = calendar.getTime();
        return lastYearDate;
    }

    /**
     * 根据当前时间返回当年每一个月
     */
    public static List<Date> get12Months(long currentTime) {
        Date currentDate = new Date(currentTime);
        //获取当前年份
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String currentYear = sdf.format(currentDate);
        //遍历创建每月的日期
        List<Date> dateList = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
            try {
                Date currnetMonth = null;
                if (i < 10) {
                    currnetMonth = dateFormat.parse(currentYear + "-0" + i);
                } else {
                    currnetMonth = dateFormat.parse(currentYear + "-" + i);
                }
                dateList.add(currnetMonth);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return dateList;
    }

    /**
     * 获取指定日期几个月后的日期
     */
    public static Date getDateAfterMonth(Date date, Integer count) {
        //date是当前日期
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //count是指定几个月后；
        calendar.add(Calendar.MONTH, count);
        Date resultDate = calendar.getTime();
        return resultDate;
    }
}
