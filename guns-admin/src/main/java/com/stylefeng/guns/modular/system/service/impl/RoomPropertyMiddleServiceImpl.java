package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.RoomPropertyMiddle;
import com.stylefeng.guns.modular.system.dao.RoomPropertyMiddleMapper;
import com.stylefeng.guns.modular.system.service.IRoomPropertyMiddleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间资产对象（例如：床，盥洗台；油烟机；）和房间对象的中间表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-07
 */
@Service
public class RoomPropertyMiddleServiceImpl extends ServiceImpl<RoomPropertyMiddleMapper, RoomPropertyMiddle> implements IRoomPropertyMiddleService {

}
