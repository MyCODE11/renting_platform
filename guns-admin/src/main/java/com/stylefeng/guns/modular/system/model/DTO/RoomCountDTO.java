package com.stylefeng.guns.modular.system.model.DTO;

import java.util.Date;

/**
 * 用于某年每月房量统计的自定义类
 */
public class RoomCountDTO {
    //月份
    private String month;
    //当月房量统计
    private Integer count;

    @Override
    public String toString() {
        return "RoomCountDTO{" +
                "month=" + month +
                ", count=" + count +
                '}';
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
