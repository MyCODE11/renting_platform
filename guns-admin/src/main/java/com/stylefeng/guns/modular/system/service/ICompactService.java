package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.Compact;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 合同信息 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-08
 */
public interface ICompactService extends IService<Compact> {

}
