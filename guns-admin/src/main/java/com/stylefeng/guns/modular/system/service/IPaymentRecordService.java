package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.PaymentRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 该表用于表示每个房间的缴费记录 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-25
 */
public interface IPaymentRecordService extends IService<PaymentRecord> {

}
