package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.DTO.RoomDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 毛坯房房间对象 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-28
 */
public interface IRawRoomService {
    /**
     * 删除房源
     */
    public void deleteRoomById(Integer roomId);

    /**
     * 用于毛坯房房源管理页面的房间对象查询
     *
     * @return
     */
    public List<RoomDTO> selectRawRoomForManage(Map<String, Object> params);

}
