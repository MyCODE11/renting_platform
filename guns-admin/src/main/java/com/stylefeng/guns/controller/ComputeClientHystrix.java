package com.stylefeng.guns.controller;

import com.stylefeng.guns.ComputeClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 总览信息
 *
 * @author king_zhang
 * @Date 2018年3月1日 下午8:25:24
 */
@Component
public class ComputeClientHystrix implements ComputeClient {
    @Override
    public Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        return -9999;
    }

    @Override
    public String sendSimpleMail(String sendTo, String titel, String content) {
        return "9999";
    }
    @Override
    public void sendMessageMail(String messageCode, String messageStatus, String cause, String title, String templateName, String senTo) {


    }
}
