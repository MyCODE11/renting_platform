package com.stylefeng.guns.controller;


import com.stylefeng.guns.ComputeClient;

import com.stylefeng.guns.core.base.controller.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 总览信息
 *
 * @author king_zhang
 * @Date 2018年3月1
 */
@RestController
public class ConsumerController extends BaseController {

    @Autowired
    ComputeClient computeClient;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Integer add() {
        return computeClient.add(10, 20);
    }

    /**
     * 测试邮件发送
     */
    @ApiOperation(value="测试邮件发送", notes="getEntityById")
    @RequestMapping(value = "/getTestDemoEmail", method = RequestMethod.GET)
    public String getEntityById() throws Exception {
            String sendTo = "947826624@qq.com";
            String titel = "测试邮件标题";
            String content = "测试邮件内容";
            computeClient.sendSimpleMail(sendTo, titel, content);
        return "200";
    }

    /**
     * 邮件模板
     * @param
     * @param title
     * @param templateName
     * @param senTo
     */
    @ApiOperation(value="模板邮件发送", notes="getEntityById")
    @RequestMapping(value = "/sendMessage", method = RequestMethod.GET)
    public void sendMessageMail(String messageCode,String messageStatus,String cause,String title, String templateName, String senTo) {
        computeClient.sendMessageMail(messageCode,messageStatus,cause,title,templateName,senTo);
    }

}
